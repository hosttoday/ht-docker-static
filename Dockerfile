FROM registry.gitlab.com/hosttoday/ht-docker-node:stable
COPY ./default /serve
RUN npm install -g @servezone/staticly
WORKDIR /serve
CMD [ "staticly" ] 
EXPOSE 3000
