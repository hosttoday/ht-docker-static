#!/usr/bin/env bash
docker build --no-cache -t staticimg .
docker stop static
docker rm $(docker ps -q -f status=exited)
docker run -d -p 3000:8080 --name static staticimg staticly --port 8080
